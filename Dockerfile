FROM php:7.2-fpm

ADD config/packages.blackfire.io.gpg.key /tmp

RUN apt update \
    && apt install -y --no-install-recommends \
    software-properties-common \
    gnupg \
    gpg-agent \
    && cat /tmp/packages.blackfire.io.gpg.key | apt-key add - \
    && echo "deb http://packages.blackfire.io/debian any main" | tee /etc/apt/sources.list.d/blackfire.list \
    && rm /tmp/packages.blackfire.io.gpg.key \
    && apt update \
    && DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends \
    supervisor \
    openssh-client \
    git \
    librsvg2-2 \
    libxml2 \
    imagemagick \
    ca-certificates \
    xz-utils \
    blackfire-php \
    # needed for gd
    libpng-dev \
    # needed for intl
    libicu-dev \
    # needed for imagick
    libmagickwand-dev \
    # needed for imagic
    libmagickcore-dev \
    && docker-php-ext-install -j$(nproc) sockets \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install -j$(nproc) zip \
    && docker-php-ext-configure intl \
    && docker-php-ext-install -j$(nproc) intl \
    && docker-php-ext-install -j$(nproc) bcmath \
    && docker-php-ext-install -j$(nproc) opcache \
    && docker-php-ext-install -j$(nproc) wddx \
    && docker-php-ext-install -j$(nproc) gettext \
    && docker-php-ext-install -j$(nproc) shmop \
    && docker-php-ext-install -j$(nproc) sysvmsg \
    && docker-php-ext-install -j$(nproc) sysvsem \
    && docker-php-ext-install -j$(nproc) sysvshm \
    && pecl install imagick-3.4.3 \
    && pecl install redis-4.1.1 \
    && pecl install apcu-5.1.12 \
    && pecl install igbinary-2.0.8 \
    && docker-php-ext-enable imagick redis apcu igbinary \
    && rm -rf /var/lib/apt/lists/*
